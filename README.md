# React Native Boilerplate with Redux and React Navigation Architecture Powered by NativeBase
*By Sam Chen*

1. To install, make sure you first have a working react-native development environment building with native code.
[See React-Native Getting Started *Building Projects With Native Code*](https://facebook.github.io/react-native/docs/getting-started.html)
2. Clone this repo:`git clone https://MintyMedicine@bitbucket.org/MintyMedicine/rntemplate.git
`
3. Install Dependencies: `npm install`
